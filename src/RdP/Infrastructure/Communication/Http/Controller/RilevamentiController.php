<?php

namespace RdP\Infrastructure\Communication\Http\Controller;

use Exception;
use InvalidArgumentException;
use RdP\Domain\Service\CreaRilevamento;
use RdP\Domain\Service\CreaRilevamentoRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class RilevamentiController
{
    public function index(Request $request, CreaRilevamento $creaRilevamento)
    {
        $response = new JsonResponse(
            [
                'status' => 'u '. time()
            ]
        );

        return $response;
    }

    public function store(Request $request, CreaRilevamento $creaRilevamento)
    {
        if ($request->getContentType() != 'json') {
            throw new Exception(sprintf("Wrong content type [%s]", $request->getContentType()));
        }
        $richiestaArray = json_decode($request->getContent(), true);



        $response = new JsonResponse();

        try {
            $rilevamentoResponse = $creaRilevamento->crea(new CreaRilevamentoRequest($richiestaArray));

            if ($rilevamentoResponse->isSuccess()) {
                $message = [
                    'status' => 'success',
                    'id' => $rilevamentoResponse->message()
                ];
            } else {
                $response->setStatusCode(400);
                $message = [
                    'status' => 'fail',
                    'message' => $rilevamentoResponse->message(),
                    'errors' => $rilevamentoResponse->errors()
                ];
            }
        } catch (InvalidArgumentException $e) {
            $response->setStatusCode(400);
            $message = [
                'status' => 'fail',
                'message' => $e->getMessage(),
                'errors' => explode(PHP_EOL, $e->getTraceAsString())
            ];
        }

        $response->setData($message);

        return $response;
    }
}

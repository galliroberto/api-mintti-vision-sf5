<?php

declare(strict_types=1);

namespace RdP\Infrastructure\Communication\Command;

use Faker\Factory;
use Faker\Provider\it_IT\Address;
use Phalcon\Cli\Task;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\Service\CreaRilevamento;
use RdP\Domain\Service\CreaRilevamentoRequest;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Sesso;
use RdP\Infrastructure\Domain\DoctrineRilevamenti;

class FixtureCommand extends Task
{
    public function mainAction()
    {
        echo 'Per lanciare le fixture esegui php cli.php fixture run [limit]' . PHP_EOL;
    }

    public function runAction(int $limit)
    {
        $this->faker = Factory::create();
        $this->faker->addProvider(new Address($this->faker));

        /*
         *
            drop table device;
            drop table soggetto;
            drop table misura_temperatura;
            drop table misura_pressione;
            drop table phalcon_migrations;
         */

        /*
            truncate table device;
            truncate table soggetto;
            truncate table misura_temperatura;
            truncate table misura_pressione;
            truncate table phalcon_migrations;
         */

        for ($i = 0; $i < $limit; $i++) {
            $request = [
                "device" => $this->deviceFaker(),
                "soggetto" => $this->soggettoFaker(),
                "misure" => $this->misureFaker(),
            ];

            $creaRilevamentoRequest = new CreaRilevamentoRequest($request);

            $creaRilevamento = new CreaRilevamento(new DoctrineRilevamenti());
            $creaRilevamento->crea($creaRilevamentoRequest);
        }
    }

    private function deviceFaker()
    {
        return [
            "info" => ["chiave info" => "valore info"],
            "occurred_at" => $this->faker->dateTimeBetween($startDate = '-3 days', $endDate = 'now', $timezone = null)->format("Y-m-d H:i:s")
        ];
    }

    private function misureFaker(): array
    {
        if ($this->faker->boolean) {
            $misure = [
                "pressione" => [
                    "sistolica" => $this->faker->numberBetween(100, 150),
                    "diastolica" => $this->faker->numberBetween(50, 90),
                    "battiti" => $this->faker->numberBetween(40, 150),
                    "occurred_at" => $this->faker->dateTimeBetween($startDate = '-3 days', $endDate = 'now', $timezone = null)->format("Y-m-d H:i:s")
                ],
            ];
        } else {
            $misure = [
                "temperatura" => [
                    "temperatura" => $this->faker->randomFloat(1, 35, 41),
                    "unita_misura" => Temperatura::SCALA_CELSIUS,
                    "occurred_at" => $this->faker->dateTimeBetween($startDate = '-3 days', $endDate = 'now', $timezone = null)->format("Y-m-d H:i:s")
                ]
            ];
        }

        return $misure;
    }

    private function soggettoFaker(): array
    {
        return [
            "sesso" => $this->faker->randomElement([Sesso::SESSO_M, Sesso::SESSO_F, Sesso::SESSO_X]),
            "eta" => $this->faker->randomElement([
                Eta::ETA_FASCIA_10_20,
                Eta::ETA_FASCIA_21_30,
                Eta::ETA_FASCIA_31_40,
                Eta::ETA_FASCIA_41_50,
                Eta::ETA_FASCIA_51_60,
                Eta::ETA_FASCIA_61_70,
                Eta::ETA_FASCIA_71_INF
            ]),
            "posizione" => [
                "latitude" => $this->faker->latitude(),
                "longitude" => $this->faker->longitude()
            ]
        ];
    }
}
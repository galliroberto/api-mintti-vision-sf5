<?php
namespace RdP\Domain\ValueObject;

final class Posizione
{
    private float $latitude;
    private float $longitude;

    private function __construct(float $latitude, float $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public static function crea(float $latitude, float $longitude): self
    {
        return new self($latitude, $longitude);
    }

    public function latitude(): float
    {
        return $this->latitude;
    }

    public function longitude(): float
    {
        return $this->longitude;
    }

    public function toArray(): array {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
        ];
    } 

    public function __toClone():void {}
}
<?php

namespace RdP\Domain\ValueObject;

use InvalidArgumentException;

final class Sesso
{
    public const SESSO_M = 'm';
    public const SESSO_F = 'f';
    public const SESSO_X = 'x';
    private string $sesso;

    private function __construct(string $sesso)
    {
        $this->sessoValidoOrFail($sesso);

        $this->sesso = $sesso;
    }

    public static function crea(string $sesso): self
    {
        return new self($sesso);
    }

    private function sessoValidoOrFail(string $sesso): void
    {
        if (!in_array($sesso, [self::SESSO_M, self::SESSO_F, self::SESSO_X])) {
            throw new InvalidArgumentException('Sesso Non valido');
        }
    }

    public function sesso(): string
    {
        return $this->sesso;
    }

    public function __toClone(): void
    {
    }
}
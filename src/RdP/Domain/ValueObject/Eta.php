<?php

namespace RdP\Domain\ValueObject;

use InvalidArgumentException;

final class Eta
{
    public const ETA_FASCIA_10_20 = 12;
    public const ETA_FASCIA_21_30 = 23;
    public const ETA_FASCIA_31_40 = 34;
    public const ETA_FASCIA_41_50 = 45;
    public const ETA_FASCIA_51_60 = 56;
    public const ETA_FASCIA_61_70 = 67;
    public const ETA_FASCIA_71_INF = 70;
    private string $eta;

    private function __construct(int $eta)
    {
        $this->etaValidaOrFail($eta);

        $this->eta = $eta;
    }

    public static function crea(int $eta): self
    {
        return new self($eta);
    }

    private function etaValidaOrFail(int $eta): void
    {
        if (!in_array($eta, [
            self::ETA_FASCIA_10_20,
            self::ETA_FASCIA_21_30,
            self::ETA_FASCIA_31_40,
            self::ETA_FASCIA_41_50,
            self::ETA_FASCIA_51_60,
            self::ETA_FASCIA_61_70,
            self::ETA_FASCIA_71_INF
        ])) {
            throw new InvalidArgumentException('Età Non valida');
        }
    }

    public function eta(): int
    {
        return $this->eta;
    }

    public function __toClone(): void
    {
    }
}
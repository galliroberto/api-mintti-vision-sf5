<?php

namespace RdP\Domain\Aggregate;

use RdP\Domain\Aggregate\Misura\Misure;
use RdP\Domain\Aggregate\Misura\Pressione;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\ValueObject\MisurazioneParametri;
use RdP\Domain\ValueObject\RilevamentoId;

final class Rilevamento
{
    const TEMPERATURA = 'temperatura';
    const PRESSIONE = 'pressione';
    private array $misureConsentite = [
        self::TEMPERATURA,
        self::PRESSIONE,
    ];

    private RilevamentoId $id;
    private Device $device;
    private Soggetto $soggetto;

    private array $misure;

    private Pressione $pressione;
    private Temperatura $temperatura;

    private function __construct(
        RilevamentoId $id,
        array $device,
        array $soggetto,
        array $misure
    ) {
        $this->id = $id;

        $this->creaDevice($device);
        $this->creaSoggetto($soggetto);
        $this->creaMisure($misure);
    }

    public static function crea(
        RilevamentoId $id,
        array $device,
        array $soggetto,
        array $misure
    ): self {
        return new self($id, $device, $soggetto, $misure);
    }

    private function creaDevice($device): void
    {
        $this->device = Device::crea($this->id, $device['info'], $device['occurred_at']);
    }

    private function creaSoggetto($soggetto): void
    {
        $this->soggetto = Soggetto::crea($this->id, $soggetto['sesso'], $soggetto['eta'], $soggetto['posizione'], $soggetto['occurred_at']);
    }

    private function creaMisure(array $misureRequest): void
    {
        foreach ($misureRequest as $nomeMisura => $parametriMisura) {
            $this->aggiungiMisura($nomeMisura, $parametriMisura);
        }
    }

    private function aggiungiMisura(string $nomeMisura, array $parametriMisura): void
    {
        switch ($nomeMisura) {
            case self::TEMPERATURA:
                $this->temperatura = $misura = Temperatura::crea($this->id, $parametriMisura['temperatura'], $parametriMisura['unita_misura'],
                    $parametriMisura['occurred_at']);

                break;
            case self::PRESSIONE:
                $this->pressione = $misura = Pressione::crea($this->id, $parametriMisura['sistolica'], $parametriMisura['diastolica'], $parametriMisura['battiti'],
                    $parametriMisura['occurred_at']);

                break;
            default:
                throw new InvalidArgumentException(sprintf('Misura non valida: %s', $nomeMisura));
                break;
        }

        $this->misure[$nomeMisura] = $misura;
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function device(): Device
    {
        return $this->device;
    }

    public function soggetto(): Soggetto
    {
        return $this->soggetto;
    }

    public function misure(): Misure
    {
        return $this->misure;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->toString(),
            'device' => $this->device->toArray(),
            'soggetto' => $this->soggetto->toArray(),
            'misure' => $this->misure->toArray(),
        ];
    }
}
<?php

namespace RdP\Domain\Aggregate\Misura;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Rilevamento;
use RdP\Domain\ValueObject\RilevamentoId;

abstract class Misura
{
    protected RilevamentoId $id;
    protected DateTimeImmutable $occurredAt;

    public function getNomeMisura(): string
    {
        return static::NOME_MISURA;
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function isTemperatura(): bool
    {
        return Rilevamento::TEMPERATURA == static::NOME_MISURA;
    }

    public function isPressione(): bool
    {
        return Rilevamento::PRESSIONE == static::NOME_MISURA;
    }
}
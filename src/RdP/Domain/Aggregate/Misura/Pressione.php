<?php

namespace RdP\Domain\Aggregate\Misura;

use DateTimeImmutable;
use InvalidArgumentException;
use RdP\Domain\ValueObject\RilevamentoId;

class Pressione extends Misura
{
    protected const NOME_MISURA = 'pressione';
    protected const PRESSIONE_MINIMA = 50;
    protected const PRESSIONE_MASSIMA = 200;
    protected const BATTITI_MINIMI = 30;
    protected const BATTITI_MASSIMI = 200;

    private int $sistolica;
    private int $diastolica;
    private int $battiti;

    private function __construct(
        RilevamentoId $id,
        int $sistolica,
        int $diastolica,
        int $battiti,
        DateTimeImmutable $occurredAt
    ) {
        $this->validaDatiRilevatiOrFail($sistolica, $diastolica, $battiti);

        $this->id = $id;
        $this->sistolica = $sistolica;
        $this->diastolica = $diastolica;
        $this->battiti = $battiti;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(
        RilevamentoId $id,
        int $sistolica,
        int $diastolica,
        int $battiti,
        DateTimeImmutable $occurredAt
    ): self {
        return new self($id, $sistolica, $diastolica, $battiti, $occurredAt);
    }

    private function validaDatiRilevatiOrFail(
        int $sistolica,
        int $diastolica,
        int $battiti
    ): void {
        if (!in_array($sistolica, range(self::PRESSIONE_MINIMA, self::PRESSIONE_MASSIMA))) {
            throw new InvalidArgumentException(sprintf("Sistolica fuori range. Ammessi valori compresi fra [%s] e [%s]", self::PRESSIONE_MINIMA,
                self::PRESSIONE_MASSIMA));
        }

        if (!in_array($diastolica, range(self::PRESSIONE_MINIMA, self::PRESSIONE_MASSIMA))) {
            throw new InvalidArgumentException(sprintf("Diastolica fuori range. Ammessi valori compresi fra [%s] e [%s]", self::PRESSIONE_MINIMA,
                self::PRESSIONE_MASSIMA));
        }

        if ($sistolica < $diastolica) {
            throw new InvalidArgumentException("La pressione sistolica deve essere maggiore della diastolica");
        }

        if (!in_array($battiti, range(self::BATTITI_MINIMI, self::BATTITI_MASSIMI))) {
            throw new InvalidArgumentException(sprintf("Battiti fuori range. Ammessi valori compresi fra [%s] e [%s]", self::BATTITI_MINIMI,
                self::BATTITI_MASSIMI));
        }
    }

    public function sistolica(): int
    {
        return $this->sistolica;
    }

    public function diastolica(): int
    {
        return $this->diastolica;
    }

    public function battiti(): int
    {
        return $this->battiti;
    }

    public function toArray(): array
    {
        return [
            'sistolica' => $this->sistolica,
            'diastolica' => $this->diastolica,
            'battiti' => $this->battiti,
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}
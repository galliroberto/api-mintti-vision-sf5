<?php

namespace RdP\Domain\Aggregate\Misura;

use DateTimeImmutable;
use InvalidArgumentException;
use RdP\Domain\ValueObject\RilevamentoId;

class Temperatura extends Misura
{
    protected const NOME_MISURA = 'temperatura';
    public const SCALA_CELSIUS = 'C';
    public const SCALA_KELVIN = 'K';
    public const SCALA_FAHRENHEIT = 'F';
    public const TEMPERATURA_MINIMA = 30.0;
    public const TEMPERATURA_MASSIMA = 45.0;
    private float $temperatura;
    private string $unitaMisura;

    private function __construct(
        RilevamentoId $id,
        float $temperatura,
        string $unitaMisura,
        DateTimeImmutable $occurredAt
    ) {
        $this->temperaturaValidaOrFail($temperatura, $unitaMisura);

        $this->id = $id;
        $this->temperatura = $temperatura;
        $this->unitaMisura = $unitaMisura;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(
        RilevamentoId $id,
        float $temperatura,
        string $unitaMisura,
        DateTimeImmutable $occurredAt
    ): self {
        return new self($id, $temperatura, $unitaMisura, $occurredAt);
    }

    private function temperaturaValidaOrFail(float $temperatura, string $unitaMisura): void
    {
        if (!in_array($unitaMisura, [self::SCALA_CELSIUS, self::SCALA_KELVIN, self::SCALA_FAHRENHEIT])) {
            throw new InvalidArgumentException(sprintf("Scala della temperatura non valida. Valori ammessi [%s] [%s] [%s]", self::SCALA_CELSIUS,
                self::SCALA_FAHRENHEIT, self::SCALA_KELVIN));
        }

        if (!in_array($temperatura, range(self::TEMPERATURA_MINIMA, self::TEMPERATURA_MASSIMA, 0.1))) {
            throw new InvalidArgumentException(sprintf("Temperatura fuori range. Ammessi valori compresi fra [%s] e [%s]", self::TEMPERATURA_MINIMA,
                self::TEMPERATURA_MASSIMA));
        }
    }

    public function temperatura(): float
    {
        return $this->temperatura;
    }

    public function unitaMisura(): string
    {
        return $this->unitaMisura;
    }

    public function toArray(): array
    {
        return [
            'temperatura' => $this->temperatura,
            'unita_misura' => $this->unitaMisura,
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}
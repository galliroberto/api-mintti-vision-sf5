<?php

namespace RdP\Domain\Aggregate;

use DateTimeImmutable;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Posizione;
use RdP\Domain\ValueObject\RilevamentoId;
use RdP\Domain\ValueObject\Sesso;

class Soggetto
{
    private RilevamentoId $id;
    private Sesso $sesso;
    private Eta $eta;
    private Posizione $posizione;
    private DateTimeImmutable $occurredAt;

    private function __construct(
        RilevamentoId $id,
        Sesso $sesso,
        Eta $eta,
        Posizione $posizione,
        DateTimeImmutable $occurredAt
    ) {
        $this->id = $id;
        $this->sesso = $sesso;
        $this->eta = $eta;
        $this->posizione = $posizione;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(
        RilevamentoId $id,
        Sesso $sesso,
        Eta $eta,
        Posizione $posizione,
        DateTimeImmutable $occurredA
    ): self {
        return new self($id, $sesso, $eta, $posizione, $occurredA);
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function sesso(): Sesso
    {
        return $this->sesso;
    }

    public function eta(): Eta
    {
        return $this->eta;
    }

    public function posizione(): Posizione
    {
        return $this->posizione;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->toArray(),
            'sesso' => $this->sesso->toArray(),
            'eta' => $this->eta->toArray(),
            'posizione' => $this->posizione->toArray(),
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}
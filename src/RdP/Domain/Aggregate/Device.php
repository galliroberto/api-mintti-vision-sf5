<?php

namespace RdP\Domain\Aggregate;

use DateTimeImmutable;
use RdP\Domain\ValueObject\RilevamentoId;

final class Device
{
    private RilevamentoId $id;
    private array $info;
    private DateTimeImmutable $occurredAt;

    private function __construct(RilevamentoId $id, array $info, DateTimeImmutable $occurredAt)
    {
        $this->id = $id;
        $this->info = $info;
        $this->occurredAt = $occurredAt;
    }

    public static function crea(RilevamentoId $id, array $info, DateTimeImmutable $occurredAt): self
    {
        return new self($id, $info, $occurredAt);
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function info(): array
    {
        return $this->info;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id->toString(),
            'info' => $this->info,
            'occurred_at' => $this->occurredAt->format('Y-m-d H:i:s'),
        ];
    }
}
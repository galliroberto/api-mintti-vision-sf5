<?php

namespace RdP\Domain\Service;

use DateTimeImmutable;
use InvalidArgumentException;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Posizione;
use RdP\Domain\ValueObject\RilevamentoId;
use RdP\Domain\ValueObject\Sesso;
use Throwable;

class CreaRilevamentoRequest
{
    private RilevamentoId $id;
    private array $device;
    private array $soggetto;
    private array $misure;

    //private DateTimeImmutable $occurredAt;

    public function __construct(array $request)
    {
        try {
            $this->id = RilevamentoId::crea();
            $this->creaDevice($request['device']);
            $this->creaSoggetto($request['soggetto']);
            $this->creaMisure($request['misure']);
        } catch (Throwable $t) {
            throw new InvalidArgumentException(sprintf("parametri non validi. [%s] ", $t->getMessage()));
        }
    }

    private function creaDevice($device): void
    {
        $info = (array)$device['info'];
        $occurredAt = new DateTimeImmutable($device['occurred_at']);

        $this->device = [
            'info' => $info,
            'occurred_at' => $occurredAt,
        ];
    }

    private function creaSoggetto($soggetto): void
    {
        $sesso = Sesso::crea($soggetto['sesso']);
        $eta = Eta::crea($soggetto['eta']);
        $posizione = Posizione::crea($soggetto['posizione']['latitude'], $soggetto['posizione']['longitude']);
        $occurredAt = new DateTimeImmutable($soggetto['occurred_at']);

        $this->soggetto = [
            'sesso' => $sesso,
            'eta' => $eta,
            'posizione' => $posizione,
            'occurred_at' => $occurredAt,
        ];
    }

    private function creaMisure(array $misureRequest): void
    {
        foreach ($misureRequest as $nomeMisura => $parametriMisura) {
            $parametriMisura['occurred_at'] = new DateTimeImmutable($parametriMisura['occurred_at']);
            $this->misure[$nomeMisura] = $parametriMisura;
        }
    }

    public function id(): RilevamentoId
    {
        return $this->id;
    }

    public function device(): array
    {
        return $this->device;
    }

    public function soggetto(): array
    {
        return $this->soggetto;
    }

    public function misure(): array
    {
        return $this->misure;
    }
}
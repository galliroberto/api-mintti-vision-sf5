<?php

namespace App\EventSubscriber;


use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Jsor\Doctrine\PostGIS\Event\ORMSchemaEventSubscriber;

class PostGIS_Lib_VersionSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // perhaps you only want to act on some "Product" entity
        if ($entity instanceof Cible) {
            $entityManager = $args->getObjectManager();
            // ... do something with the Product
            $entityManager->getEventManager()->addEventSubscriber(new ORMSchemaEventSubscriber());
        }
    }
}
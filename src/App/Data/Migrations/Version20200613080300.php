<?php

declare(strict_types=1);

namespace App\Data\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200613080300 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE rilevamento (id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN rilevamento.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE soggetto ALTER posizione_longitude TYPE NUMERIC(16, 14)');
        $this->addSql('ALTER TABLE soggetto ALTER posizione_latitude TYPE NUMERIC(16, 14)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE rilevamento');
        $this->addSql('ALTER TABLE soggetto ALTER posizione_longitude TYPE NUMERIC(14, 0)');
        $this->addSql('ALTER TABLE soggetto ALTER posizione_latitude TYPE NUMERIC(14, 0)');
    }
}

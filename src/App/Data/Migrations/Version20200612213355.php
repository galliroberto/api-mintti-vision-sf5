<?php

declare(strict_types=1);

namespace App\Data\DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200612213355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE soggetto (id UUID NOT NULL, occurred_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sesso_sesso VARCHAR(1) NOT NULL, eta_eta INT NOT NULL, posizione_longitude NUMERIC(14, 0) DEFAULT NULL, posizione_latitude NUMERIC(14, 0) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN soggetto.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN soggetto.occurred_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE misura_temperatura (id UUID NOT NULL, temperatura INT NOT NULL, unita_misura VARCHAR(1) NOT NULL, occurred_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN misura_temperatura.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN misura_temperatura.occurred_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE device (id UUID NOT NULL, info JSON DEFAULT NULL, occurred_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN device.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN device.occurred_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE misura_pressione (id UUID NOT NULL, sistolica INT NOT NULL, diastolica INT NOT NULL, battiti INT NOT NULL, occurred_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN misura_pressione.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN misura_pressione.occurred_at IS \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE soggetto');
        $this->addSql('DROP TABLE misura_temperatura');
        $this->addSql('DROP TABLE device');
        $this->addSql('DROP TABLE misura_pressione');
    }
}

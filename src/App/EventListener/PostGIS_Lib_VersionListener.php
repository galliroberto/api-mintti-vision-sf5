<?php

namespace App\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;


use Jsor\Doctrine\PostGIS\Event\DBALSchemaEventSubscriber;

class PostGIS_Lib_VersionListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "Product" entity
        if (!$entity instanceof Cible || !$entity instanceof Ciblegeocodee || !$entity instanceof Iris) {
            return;
        }

        $entityManager = $args->getObjectManager();

        // La doc github dit $connexion mais je ne sait pas à quoi correspond cette variable
        $connection->getEventManager()->addEventSubscriber(new DBALSchemaEventSubscriber());
    }
}
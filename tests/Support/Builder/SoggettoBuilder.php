<?php

namespace Tests\Support\Builder;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Soggetto;
use RdP\Domain\ValueObject\Eta;
use RdP\Domain\ValueObject\Posizione;
use RdP\Domain\ValueObject\RilevamentoId;
use RdP\Domain\ValueObject\Sesso;

final class SoggettoBuilder
{
    private RilevamentoId $id;
    private Sesso $genere;
    private Eta $eta;
    private Posizione $geoLocation;
    private DateTimeImmutable $occurredAt;

    protected function __construct()
    {
        $this->id = RilevamentoId::crea();
        $this->genere = SessoBuilder::crea()->build();
        $this->eta = EtaBuilder::crea()->build();
        $this->geoLocation = PosizioneBuilder::crea()->build();
        $this->occurredAt = new DateTimeImmutable();
    }

    public static function crea(): self
    {
        return new static();
    }

    public function withGenere(Sesso $genere): self
    {
        $this->genere = $genere;

        return $this;
    }

    public function withEta(Eta $eta): self
    {
        $this->eta = $eta;

        return $this;
    }

    public function build(): Soggetto
    {
        return Soggetto::crea($this->id, $this->genere, $this->eta, $this->geoLocation, $this->occurredAt);
    }
}
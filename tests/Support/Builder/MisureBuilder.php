<?php

namespace Tests\Support\Builder;

use RdP\Domain\Aggregate\Misura\Misure;
use RdP\Domain\ValueObject\MisurazioneParametri;

final class MisureBuilder
{
    private array $misure;

    protected function __construct()
    {
        $this->misure = [];
    }

    public static function crea(): self
    {
        return new static();
    }

    public function withTemperatura(Temperatura $temperatura): self
    {
        $this->misure[] = $temperatura;

        return $this;
    }

    public function withPressione(Pressione $pressione): self
    {
        $this->misure[] = $pressione;

        return $this;
    }

    public function build(): Misure
    {
        $misure = Misure::crea();
        foreach ($this->misure as $misura) {
            $misure->aggiungiMisura($misura);
        }

        return $misure;
    }
}
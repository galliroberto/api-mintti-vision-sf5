<?php

namespace Tests\Support\Builder;


use RdP\Domain\ValueObject\Posizione;

class PosizioneBuilder
{
    private float $latitude;
    private float $longitude;

    protected function __construct()
    {
        $this->latitude = 12.5;
        $this->longitude = 52.5;
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Posizione
    {
        return Posizione::crea($this->latitude, $this->longitude);
    }
}
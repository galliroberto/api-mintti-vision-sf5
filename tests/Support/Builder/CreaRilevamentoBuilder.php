<?php

namespace Tests\Support\Builder;

use RdP\Domain\Service\CreaRilevamentoRequest;

final class CreaRilevamentoBuilder
{
    private array $request;

    protected function __construct()
    {
        $this->request = json_decode(
            '{
                    "device": {
                        "info": {
                            "battery": 42
                        },
                        "occurred_at": "2020-05-20 15:01:00"
                    },
                    "soggetto": {
                        "sesso": "m",
                        "eta": 23,
                        "posizione": {
                            "latitude": 12,
                            "longitude": 52
                        },
                        "occurred_at": "2020-05-20 15:03:00"
                    },
                    "misure": {
                        "pressione": {
                            "sistolica": 130,
                            "diastolica": 80,
                            "battiti": 57,
                            "occurred_at": "2020-05-20 15:05:00"
                        },
                        "temperatura": {
                            "temperatura": 37.5,
                            "unita_misura": "C",
                            "occurred_at": "2020-05-20 15:07:00"
                        }
                    }
                }',
            true
        );
    }

    public static function crea(): self
    {
        return new static();
    }

    public function withRequest(array $request): self {
        $this->request = $request;

        return $this;
    }

    public function build(): CreaRilevamentoRequest
    {
        return new CreaRilevamentoRequest($this->request);
    }
}
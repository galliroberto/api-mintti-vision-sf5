<?php

namespace Tests\Support\Builder;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\ValueObject\MisurazioneParametri;
use RdP\Domain\ValueObject\RilevamentoId;

final class TemperaturaBuilder
{

    private RilevamentoId $id;
    private float $temperatura;
    private string $unitaMisura;
    private DateTimeImmutable $occurredAt;

    protected function __construct()
    {
        $this->id = RilevamentoId::crea();
        $this->temperatura = 36.8;
        $this->unitaMisura = 'C';
        $this->occurredAt = new DateTimeImmutable();
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Temperatura
    {
        return Temperatura::crea($this->id, $this->temperatura, $this->unitaMisura, $this->occurredAt);
    }
}
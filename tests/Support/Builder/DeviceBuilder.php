<?php

namespace Tests\Support\Builder;

use DateTimeImmutable;
use RdP\Domain\Aggregate\Device;
use RdP\Domain\ValueObject\RilevamentoId;

final class DeviceBuilder
{
    private RilevamentoId $id;
    private array $info;
    private DateTimeImmutable $occurredAt;

    protected function __construct()
    {
        $this->id = RilevamentoId::crea();
        $this->info = [];
        $this->occurredAt = new DateTimeImmutable();
    }

    public static function crea(): self
    {
        return new static();
    }

    public function withInfo(array $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function build(): Device
    {
        return Device::crea($this->id, $this->info, $this->occurredAt);
    }
}
<?php

namespace Tests\Support\Builder;

use RdP\Domain\ValueObject\Sesso;

class SessoBuilder
{
    private string $genere;

    protected function __construct()
    {
        $this->genere = Sesso::SESSO_M;
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Sesso
    {
        return Sesso::crea($this->genere);
    }
}
<?php

namespace Tests\RdP\Domain\ValueObject;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RdP\Domain\ValueObject\Sesso;
use TypeError;

final class SessoTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametriValidiProvider
     */
    public function crea_genere_con_parametri_validi($genere): void
    {
        $genereVO = Sesso::crea($genere);

        $this->assertEquals($genere, $genereVO->sesso());
    }

    public function parametriValidiProvider(): array
    {
        return [
            ['m'],
            ['f'],
            ['x'],
        ];
    }

    /**
     * @test
     * @dataProvider parametriNonValidiProvider
     */
    public function crea_genere_con_parametri_non_validi($genere): void
    {
        $this->expectException(InvalidArgumentException::class);

        Sesso::crea($genere);
    }

    public function parametriNonValidiProvider(): array
    {
        return [
            ['l'],
            ['lalal'],
            [0],
            [false]
        ];
    }

    /**
     * @test
     * @dataProvider parametriTypeErrorProvider
     */
    public function crea_genere_con_parametri_type_error($genere): void
    {
        $this->expectException(TypeError::class);

        Sesso::crea($genere);
    }

    public function parametriTypeErrorProvider(): array
    {
        return [
            [null],
        ];
    }
}

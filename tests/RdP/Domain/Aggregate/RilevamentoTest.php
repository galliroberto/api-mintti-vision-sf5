<?php

namespace Tests\RdP\Domain;

use Phalcon\DI;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Rilevamento;
use RdP\Domain\ValueObject\RilevamentoId;
use Tests\Support\Builder\DeviceBuilder;
use Tests\Support\Builder\MisureBuilder;
use Tests\Support\Builder\SoggettoBuilder;

class RilevamentoTest extends TestCase
{
    /**
     * @test
     */
    public function crea_rilevamento(): void
    {
        $rilevamentoId = RilevamentoId::crea();

        $rilevamento = Rilevamento::crea($rilevamentoId, DeviceBuilder::crea()->build(), SoggettoBuilder::crea()->build(),
            MisureBuilder::crea()->build());

        $this->assertEquals($rilevamentoId, $rilevamento->id());
    }
}

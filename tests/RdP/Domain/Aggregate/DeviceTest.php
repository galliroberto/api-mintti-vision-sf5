<?php

namespace Tests\RdP\Domain;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Device;
use RdP\Domain\ValueObject\RilevamentoId;

class DeviceTest extends TestCase
{
    /**
     * @test
     */
    public function crea_device(): void
    {
        $id = RilevamentoId::crea();
        $info = [];
        $occurredAt = new DateTimeImmutable();

        $device = Device::crea($id, [], $occurredAt);

        $this->assertEquals($info, $device->info());
        $this->assertEquals($occurredAt, $device->occurredAt());
    }
}

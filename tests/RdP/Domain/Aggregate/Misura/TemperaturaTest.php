<?php

namespace Tests\RdP\Domain\Aggegate\Misura;

use DateTimeImmutable;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Misura\Temperatura;
use RdP\Domain\ValueObject\MisurazioneParametri;
use RdP\Domain\ValueObject\RilevamentoId;
use TypeError;

final class TemperaturaTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametriValidiProvider
     */
    public function crea_temperatura_con_parametri_validi($valore, $unitaMisura): void
    {
        $id = RilevamentoId::crea();
        $occurredAt = new DateTimeImmutable();
        $temperatura = Temperatura::crea($id, $valore, $unitaMisura, $occurredAt);

        $this->assertEquals($valore, $temperatura->temperatura());
        $this->assertEquals($unitaMisura, $temperatura->unitaMisura());
    }

    public function parametriValidiProvider(): array
    {
        return [
            [37.5, 'C'],
            [35.8, 'F'],
            [39, 'C'],
        ];
    }

    /**
     * test
     * @dataProvider parametriNonValidiProvider
     */
    public function crea_temperatura_con_parametri_non_validi($temperatura, $unitaMisura): void
    {
        $this->expectException(InvalidArgumentException::class);

        $id = RilevamentoId::crea();
        $occurredAt = new DateTimeImmutable();
        Temperatura::crea($id, $temperatura, $unitaMisura, $occurredAt);
    }

    public function parametriNonValidiProvider(): array
    {
        return [
            [37.5, 6],
            ['test', 'F'],
            [39, false],
        ];
    }

    /**
     * test
     * @dataProvider parametriTypeErrorProvider
     */
    public function crea_temperatura_type_error($temperatura, $unitaMisura): void
    {
        $this->expectException(TypeError::class);

        $id = RilevamentoId::crea();
        $occurredAt = new DateTimeImmutable();
        Temperatura::crea($id, $temperatura, $unitaMisura, $occurredAt);
    }

    public function parametriTypeErrorProvider(): array
    {
        return [
            [null, null],
        ];
    }
}

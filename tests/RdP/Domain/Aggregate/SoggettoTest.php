<?php

namespace Tests\RdP\Domain\ValueObject;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Soggetto;
use RdP\Domain\ValueObject\RilevamentoId;
use Tests\Support\Builder\EtaBuilder;
use Tests\Support\Builder\PosizioneBuilder;
use Tests\Support\Builder\SessoBuilder;

final class SoggettoTest extends TestCase
{
    /**
     * @test
     */
    public function crea_soggetto(): void
    {
        $id = RilevamentoId::crea();
        $genere = SessoBuilder::crea()->build();
        $eta = EtaBuilder::crea()->build();
        $posizione = PosizioneBuilder::crea()->build();
        $occurredAt = new DateTimeImmutable();

        $soggetto = Soggetto::crea($id, $genere, $eta, $posizione, $occurredAt);

        $this->assertEquals($genere, $soggetto->sesso());
        $this->assertEquals($eta, $soggetto->eta());
        $this->assertEquals($posizione, $soggetto->posizione());
    }
}

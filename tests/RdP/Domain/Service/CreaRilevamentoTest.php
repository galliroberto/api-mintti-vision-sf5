<?php

namespace Tests\RdP\Domain\Service;

use InvalidArgumentException;
use Mockery;
use PHPUnit\Framework\TestCase;
use RdP\Domain\Aggregate\Rilevamenti;
use RdP\Domain\Aggregate\RilevamentiValidationException;
use RdP\Domain\Service\CreaRilevamento;
use Tests\Support\Builder\CreaRilevamentoBuilder;

class CreaRilevamentoTest extends TestCase
{
    /*
    private $db;

    protected function setUp(): void
    {
        $di = \Phalcon\DI::getDefault();
        $this->db = $di->getDb();
        $this->db->begin();
    }

    protected function tearDown(): void
    {
        $this->db->rollback();
    }
    */

    /**
     * @test
     */
    public function crea_rilevamento_success()
    {
        $creaRilevamentoRequest = CreaRilevamentoBuilder::crea()->build();

        $rilevamentiMock = Mockery::mock(Rilevamenti::class);
        $rilevamentiMock->shouldReceive('add');

        $creaRilevamento = new CreaRilevamento($rilevamentiMock);
        $rilevamentoResponse = $creaRilevamento->crea($creaRilevamentoRequest);

        $this->assertTrue($rilevamentoResponse->isSuccess());
        $this->assertEquals($creaRilevamentoRequest->id()->toString(), $rilevamentoResponse->message());
    }

    /**
     * @test
     */
    public function crea_rilevamento_request_non_valida()
    {
        $this->expectException(InvalidArgumentException::class);

        $creaRilevamentoRequest = CreaRilevamentoBuilder::crea()->withRequest([])->build();

        print_r($creaRilevamentoRequest); die;

        $rilevamentiMock = Mockery::mock(Rilevamenti::class);
        $rilevamentiMock->shouldReceive('add')->andThrow(new RilevamentiValidationException(''));

        $creaRilevamento = new CreaRilevamento($rilevamentiMock);
        $rilevamentoResponse = $creaRilevamento->crea($creaRilevamentoRequest);
    }
}
